package br.ucsal.bes.testequalidade20191.av2;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteSeleniumJbhave {

	public static WebDriver drive;
	@BeforeClass
	public void init (){
		
	System.setProperty("webdriver.chrome.drive", "C:/Users/200003839/Downloads/chromedriver_win32/chromedrive.exe");
	drive = new ChromeDriver();
		
	}
	
	@Test
	public void Teste(){
		
		drive.get("C:/Users/200003839/git/geometria-2019-1/geometria/src/main/java/webapp/triangulo.html");
	
	    
        
		
		
	}
	
	
	public void selecionarElemento(){
		WebElement selecionarCalculo = drive.findElement(By.linkText("Tipo de c�lculo"));
		selecionarCalculo.click();
		WebElement selecao = drive.findElement(By.name("hipotenusa"));
		selecao.click();
		
	}
	
	public void informarValorCateto1(String catetoUm){
		WebElement cateto1 = drive.findElement(By.linkText("Cateto 1"));
		cateto1.click();
		cateto1.sendKeys(catetoUm);
		
		
	}
	
	public void informarValorCateto2( String catetoDois){
		
		WebElement cateto2 = drive.findElement(By.name("Cateto 2"));
		cateto2.click();
		cateto2.sendKeys(catetoDois);
		cateto2.submit();
	}

	public String obterValor() {
		WebElement calculo = drive.findElement(By.linkText("Calcular"));
		calculo.click();
		WebElement valor = drive.findElement(By.linkText("Hipotenusa"));
		return valor.getText();
	}
}
