package br.ucsal.bes.testequalidade20191.av2;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class TesteTrianguloSteps {

	TesteSeleniumJbhave teste = new TesteSeleniumJbhave();
	
	@Given("Calculo da hipotenusa")
	public void escolherHipotenusa() {
        teste.selecionarElemento();
	}

	@When("E informo $3 para cateto1")
	public void informarCateto1(String cateto) {
		teste.informarValorCateto1(cateto);
	}

	@When("E informo $4 para cateto2 ")
	public void informarNota(String cateto) {
		teste.informarValorCateto2(cateto);
	}

	@Then("Ent�o a hipotenusa calculada ser� $5")
	public void verificarValor(String hipotenusa) {
		Assert.assertEquals(hipotenusa, teste.obterValor());

	}

}
